const zoom = 10;

var map = new ol.Map({
    target: 'map',
    layers: [
        new ol.layer.Tile({
            // if you want to use the default OSM tiles, you can just use the following line.
            // source: new ol.source.OSM()
            // using this, you can remove the attribution as it is already included.
            source: new ol.source.XYZ({
                url: 'http://tile.osm.ch/osm-swiss-style/{z}/{x}/{y}.png',
                attributions: [
                    `<a id="problem-attribution" href="http://www.openstreetmap.org/fixthemap?lat=0&lon=0&zoom=${zoom}">Report a problem</a> |`,
                    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                ] // You can use HTML in here!
            })
        }),
        createTableTennisLayer()
    ],
    view: new ol.View({
        zoom: zoom,
        center: ol.proj.fromLonLat([8.65639, 47.41330]),
        maxZoom: 21
    }),
    controls: ol.control.defaults({
        attributionOptions: ({
            collapsible: false
        })
    }),
});

function createTableTennisLayer() {
    const vectorSource = new ol.source.Vector({
        url: "https://api.npoint.io/113395de1b61b4bac50d",
        format: new ol.format.GeoJSON
    });

    return new ol.layer.Vector({
        title: 'Table Tennis',
        source: vectorSource,
        style: new ol.style.Style({
            image: new ol.style.Icon({
                src: 'img/lfmarker.png',
                anchor: [0.5, 1] // 0.5: middle on horizontal axis, 1: bottom of vertical axis.
            }),
        })
    });
}

map.on('moveend', function () {
    document.getElementById('problem-attribution').setAttribute('href', createProblemLink());
});

function createProblemLink() {
    var coords = ol.proj.transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
    var currentZoom = map.getView().getZoom();

    return `http://www.openstreetmap.org/fixthemap?lat=${coords[1]}&lon=${coords[0]}&zoom=${currentZoom}`;
}